# Checkout GitLab merge requests / GitHub pull requests locally

Inspired by [piscisaureus gist], improved to checkout only specific merge-request/pull-request.

The approach is also documented in [gitlab user documentation].

[piscisaureus gist]: https://gist.github.com/piscisaureus/3342247
[gitlab user documentation]: https://docs.gitlab.com/ee/user/project/merge_requests/reviewing_and_managing_merge_requests.html#checkout-merge-requests-locally

## Install

Copy the scripts into your `$PATH` somewhere.

Example:

```bash
$ git clone https://gitlab.com/glensc/git-mr /usr/local/opt/git-mr
Cloning into '/usr/local/opt/git-mr'...
warning: redirecting to https://gitlab.com/glensc/git-mr.git/
remote: Enumerating objects: 16, done.
remote: Counting objects: 100% (16/16), done.
remote: Compressing objects: 100% (15/15), done.
remote: Total 16 (delta 3), reused 0 (delta 0)
Unpacking objects: 100% (16/16), done.

$ ln -sv /usr/local/opt/git-mr/git-* /usr/local/bin/
'/usr/local/bin/git-mr' -> '/usr/local/opt/git-mr/git-mr'
'/usr/local/bin/git-pr' -> '/usr/local/opt/git-mr/git-pr'

$ git mr
Usage: /usr/local/bin/git-mr <request_number> [<remote_name>]

$ git pr
Usage: /usr/local/bin/git-pr <request_number> [<remote_name>]
```

Install with Homebrew (macOS) / Linuxbrew:

```bash
brew tap glensc/git-mr https://gitlab.com/glensc/git-mr
brew install git-mr
```

## Usage

- For GitLab: Use `git mr 1` to fetch merge-request 1 into `mr/1`
- For GitHub: Use `git pr 1` to fetch pull-request 1 into `pr/1`

> If your remote is not named `origin` you can pass your remote's name as the second argument:

```bash
git mr 1 upstream
```
